export let foodController = {
  getFoodInfoFromForm: () => {
    let foodName = document.getElementById("name").value;
    let foodPrice = document.getElementById("price").value;
    let foodFescription = document.getElementById("description").value;
    let food = {
      name: foodName,
      price: foodPrice,
      description: foodFescription,
    };

    return food;
  },

  showDataToForm: (food) => {
    document.getElementById("name").value = food.name;
    document.getElementById("price").value = food.price;
    document.getElementById("description").value = food.description;
  },
};
