let foodList = [];
let idFoodEdited = null;

import { foodService } from "../service/foodService.js";
import { spinnerService } from "../js/spinnerService.js";
import { foodController } from "../controller/controller.js";

let delFood = (idMonAn) => {
  spinnerService.loadingOn();
  foodService
    .delFood(idMonAn)
    .then((res) => {
      spinnerService.loadingOff();
      renderFoodService();
    })
    .catch((err) => {
      spinnerService.loadingOff();
    });
};
window.delFood = delFood;

let getFoodInfo = (idMonAn) => {
  idFoodEdited = idMonAn;
  spinnerService.loadingOn();
  foodService
    .getFoodInfo(idMonAn)
    .then((res) => {
      foodController.showDataToForm(res.data);
      spinnerService.loadingOff();
    })
    .catch((err) => {
      spinnerService.loadingOff();
    });
};
window.getFoodInfo = getFoodInfo;

let renderFood = (foodList) => {
  let contentHTML = "";
  for (let index = 0; index < foodList.length; index++) {
    let monAn = foodList[index];
    let contentTR = `<tr>
          <td>${monAn.id}</td>
          <td>${monAn.name}</td>
          <td>${monAn.price}</td>
          <td>${monAn.description}</td>
          <td>
              <button class="btn btn-danger" onclick="delFood(${monAn.id})">Xoá</button>
              <button class="btn btn-primary" onclick="getFoodInfo(${monAn.id})">Sửa</button>
          </td>
          
      </tr>`;

    contentHTML = contentHTML + contentTR;
  }

  document.getElementById("tbody_food").innerHTML = contentHTML;
};

let renderFoodService = () => {
  spinnerService.loadingOn();
  foodService
    .getFoodList()
    .then((res) => {
      foodList = res.data;
      renderFood(foodList);
      spinnerService.loadingOff();
    })
    .catch((err) => {
      spinnerService.loadingOff();
    });
};

renderFoodService();

let addFood = () => {
  let food = foodController.getFoodInfoFromForm();

  foodService
    .addNewFood(food)
    .then((res) => {
      renderFoodService();
    })
    .catch((err) => {
      renderFoodService();
    });
};
window.addFood = addFood;

let updateFood = () => {
  let food = foodController.getFoodInfoFromForm();
  spinnerService.loadingOn();

  let newFood = { ...food, id: idFoodEdited };

  foodService
    .foodUpdate(newFood)
    .then((res) => {
      spinnerService.loadingOff();
      renderFoodService();
    })
    .catch((err) => {
      spinnerService.loadingOff();
    });
};
window.updateFood = updateFood;
